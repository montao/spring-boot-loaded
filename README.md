[![Build Status](https://travis-ci.org/montao/spring-boot-loaded.svg?branch=master)](https://travis-ci.org/montao/spring-boot-loaded)

# spring-boot-loaded
Boilerplate for spring boot with spring loaded

Clone it with `git clone` and then run it with `gradle bootRun`
